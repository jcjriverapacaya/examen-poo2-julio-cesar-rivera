﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Examen.Models;

namespace Examen.Controllers
{
    public class ClienteController : Controller
    {
		// Lista de Clientes
		static List<Clientes> Clientes = new List<Clientes>();
        // GET: Cliente
        public ActionResult Registro()
        {
            return View(new Clientes());
        }

		[HttpPost]public ActionResult Registro(Clientes reg)
		{
			if (ModelState.IsValid)
			{
				Clientes.Add(reg);
				return RedirectToAction("Listar");
			}
			else
			{
				ViewBag.mensaje = "Verifique los datos";
				return View(reg);
			}


		}

		public ActionResult Listar()
		{
			return View(Clientes);
		}
    }
}
