﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Examen.Models
{
	public class Clientes
	{
		[Display(Name = "Codigo")]
		[Required(AllowEmptyStrings = false , ErrorMessage = "Ingrese el Codigo ")]
		[RegularExpression(@"^[\s]*(CLI[0-9]{3})$",ErrorMessage = "Formato CLI999")]
		public String codigo { get; set; }


		[Display(Name = "Razon Social")]
		[Required(AllowEmptyStrings = false, ErrorMessage = "Ingrese La Razon Social ")]
		public String razonSocial { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Ingrese la Fecha")]
		[DataType(DataType.Date)]
		[Display(Name = "Fecha de Inicio de Actividades")]
		public String fechaIA { get; set; }

		[Display(Name = "Pagina Web ( URL ) ")]
		[Required(AllowEmptyStrings = false, ErrorMessage = "Ingrese la pagina web ")]
		[RegularExpression(@"^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$", ErrorMessage = "Ingresa una URL Valida")]
		public String paginaWeb { get; set; }



		[Display(Name = "Direccion")]
		[MaxLength(100)]
		public String direccion { get; set; }


		[Display(Name = "Rubro de Negocio")]
		[MaxLength(500)]
		public String rubro { get; set; }

	}
}
